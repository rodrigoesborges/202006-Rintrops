#### Download dos dados na página do Caged ####

# Carrega os pacotes necessários:
library(tidyverse)
Sys.setlocale("LC_ALL", "C")

# Caminho para a base de dados:
#%20
#%C3%A7%C3%B5es

path <- "ftp://ftp.mtps.gov.br/pdet/microdados/NOVO CAGED/MovimentaÃ§Ãµes/2020/Maio"

# Parâmetros para extração dos dados:
ano <- 2019
mes <- str_pad(seq(1,5),2, pad="0")
files <- paste0("CAGEDMOV", ano,mes, ".7z")
files_path <- "dados/caged"
lapply(1:length(files), function(i){
system(paste0('"7z" e ', paste0("dados/",files[i]), ' -o',"dados/caged", ' -y'))
})
#dados <- NULL

# Loop para fazer o download, extração e importação dos dados:
lapply(1:length(files),function(i) {  
  download.file(url = paste0(path,"/",files[i]), destfile = paste0("dados/",files[i]), mode='wb', method='libcurl')
  system(paste0('"7z" e ', paste0("dados/",files[i]), ' -o', files_path[i], ' -y'))
  }
  )
  
  #descompacta do arquivo. Deve ter instalado o 7Zip no pc e colocar o caminho para o executável abaixo
  





#dados parcial intermitente de interesse raça genero classe etc
dados <- lapply(1:5,function (i) {
#  filtrodados <- c((1:4),(7:21),24,(41:42))
  #importa e junta dos arquivos em uma único data_frame:
  arqler <- paste0(files_path,"/",dir(files_path, pattern = "*.txt"))
  #print(arqler)
  temp <- read_delim(file = arqler[i],
                     delim = ";", 
                     locale = locale("pt",encoding = "utf-8"))
  print(c("dado carregado com ", ncol(temp)," colunas"))
 # temp <- temp[filtrodados] #seleciona apenas as colunas que são importantes para o estudo
#  gc(reset = T)#limpa memoria  
  temp
}
)

dados <- bind_rows(dados)


#salva o conjunto de dados em um arquivo rda:
saveRDS(dados, file = "dados/dados_2020-05.rds", compress = "gzip")

##CAGED 2020
subconjuntointeresse <- c(1,3,4,6,7,11,12,15,16,17,21)

##CAGED interesse
c("competência",
  "uf",
  "subclasse",
  "saldomovimentação",
  "idade",
  "sexo",
  "indtrabintermitente",
  "indtrabparcial",
  "raçacor"
  )

##CAGED 2019 INTERESSE
subconjuntointeresse <- c((1:4),(7:21),24,(41:42))


caged2019 <- readRDS("dados/2019-caged-dados.rds")
  
#dadosem <- NULL
##
dadosem <- lapply(1:5, fun(i) {
  #importa e junta dos arquivos em uma único data_frame:
  arqler <- paste0(files_path[i], '/', dir(files_path[i])[1])
  print(arqler)
  #temp <- read.csv(arqler, sep =  ";", dec = ".", encoding = "UTF-8")
  temp <- read_delim(file = arqler, 
                     delim = ";", 
                     locale = locale("pt",encoding = "utf-8"))
  print(paste0("dado carregado com ", ncol(temp)," colunas"))
  filtrodadosf <- filtrodados[c(-length(filtrodados),-(length(filtrodados)-1))]
#  if (ncol(temp) < 41) {
#    filtrodados <- filtrodados[c(-length(filtrodados),-(length(filtrodados)-1))]
#    temp <- temp[filtrodados] #seleciona apenas as colunas que são importantes para o estudo
#    temp$`Ind Trab Parcial` <- ""
#    temp$`Ind Trab Intermitente` <- ""
#  }
#    else {
    temp <- temp[filtrodadosf] #seleciona apenas as colunas que são importantes para o estudo  
#    }
  print(paste0("temp carregado com ",ncol(temp)," colunas"))
#  dados <- rbind(dados,temp)
  rm(temp)
  gc(reset = T) #limpa memoria
})

dadosem <- dplyr::rbind_list(dadosem)

dadosem$`Ind Trab Parcial` <- NA
dadosem$`Ind Trab Intermitente` <- NA

dados <- rbind(dados,dadosem)
saveRDS(dados, file = "CAGED/dados-2018-12.rds", compress = "gzip")
