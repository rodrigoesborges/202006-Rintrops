---
title: "Introdução ao R - Complemento - plotagem"
output:
  xaringan::moon_reader:
    lib_dir: libs
    nature:
      ratio: '16:9'
      highlightStyle: github
      highlightLines: yes
      countIncrementalSlides: no
  '': default
---
background-image: url(www/slidecaged_pops.jpg)
background-size: cover

<link rel="stylesheet" type="text/css" href="www/formacao.css">


---
  
```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, max.print = 30, digits = 4)
knitr::opts_chunk$set(fig.align = "center", fig.width = 10, fig.height = 6)
library(tidyverse)
library(readxl)
library(ggthemes)
library(forecast)
cageds <- read.csv2("dados/07_caged-2018-2019.csv")


```

# Tópicos do vídeo
  
  
1) plotagens presente no arquivo de scripts aula
  - com ggplot2
  - outros exemplos genéricos
    - plot
    - autoplot

2) Primeira vista de objeto série temporal



---
# LEMBRETES
  
  -  Funções aceitam argumentos em ordem, mesmo sem serem nomeados, ou _em qualquer ordem, desde que nomeados_
  - Utilizamos os seguintes pacotes para essa plotagem:
    - library(ggplot2) _disponível ao carregar o pacote tidyverse_
    - `library(ggthemes)` para temas
    
    - `library(forecast)` para a plotagem m. via autoplot

---
# Primeiro contato com plotagem via ggplot2

No script "dados/02_caged_csv_texto.R", tínhamos uma preparação dos dados:

```{r complemento, message=FALSE, warning=FALSE}
#Ajustar escala
#...
escala <- 10

cageds[cageds$tipo_de_trabalho == "menos.precário",3] <- 
  cageds[cageds$tipo_de_trabalho == "menos.precário",3]/escala 

### Transformar a coluna competência declarada 
#para formato data
cageds$competencia_declarada <- 
  as.Date(paste0(cageds$competencia_declarada,28),"%Y%m%d")


```
---
# Primeiro contato com plotagem via ggplot2
Em seguida, tínhamos exemplo de plotagem:
```{r message=FALSE, warning=FALSE, eval = F}
ggplot(cageds,aes(x = competencia_declarada, y = saldo_mov))+
  theme_stata()+
  geom_line(aes(colour=tipo_de_trabalho))+
  scale_color_brewer(palette =  "RdPu" )+
  scale_y_continuous(name = "Precários",labels = scales::comma_format(big.mark="."), 
                     sec.axis = sec_axis(~. * escala, name = "Menos Precários",labels = scales::comma_format(big.mark=".")))+
ggtitle("Empregos Formais Gerados por grau de precariedade do vínculo -\n 2018 e 2019 (milhares)")
  
```

---
# Primeiro contato com plotagem via ggplot2
Em seguida, tínhamos exemplo de plotagem:
```{r message=FALSE, warning=FALSE, echo = F}
ggplot(cageds,aes(x = competencia_declarada, y = saldo_mov))+
  theme_stata()+
  geom_line(aes(colour=tipo_de_trabalho))+
  scale_color_brewer(palette =  "RdPu" )+
  scale_y_continuous(name = "Precários",labels = scales::comma_format(big.mark="."), 
                     sec.axis = sec_axis(~. * escala, name = "Menos Precários",labels = scales::comma_format(big.mark=".")))+
ggtitle("Empregos Formais Gerados por grau de precariedade do vínculo -\n2018 e 2019 (milhares)")
  
```

---
background-image:url(www/camadas.png)
background-size: cover
# Gramática dos Gráficos*
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
_*aproximação inicial (primeira visão)_
---
No exemplo:

1) Dados : cageds.

2) Estéticas:
  - eixo x, eixo y e cor das séries (mapeadas a aspectos estéticos)
```{r, eval=F}
aes(x = competencia_declarada, y = saldo_mov)
...
aes(colour=tipo_de_trabalho)
```
  - títulos/etiquetas
```{r, eval=F}
 scale_y_continuous(name = "Precários"...
...
ggtitle("Empregos Formais Gerados por grau de precariedade do vínculo - 2019 (milhares)")
```  
3) Geometrias: linha desenhada `{eval = F} geom_line(aes(colour=tipo_de_trabalho))`



---
No exemplo:

4) Escalas: especificações adicionais para as seguintes (dimensões) estéticas:
  -  cor - paleta vermelho-roxo `scale_color_brewer(palette =  "RdPu" )`
  -  eixo y - nome, formato de milhares, escala secundária
  ```{r,eval = F} 
 scale_y_continuous(name = "Precários",labels = scales::comma_format(big.mark="."), 
 sec.axis = sec_axis(~. * escala,
 name = "Menos Precários",labels = scales::comma_format(big.mark=".")))  
  ```
<br/>
5) Estatísticas (não utilizado, valores brutos/pontos)
<br/>
6) Facetas (não utilizados, apenas 1 faceta/gráfico do conjunto)
<br/>
7) Temas: `theme_stata()`

---
#Exercícios:
<br/><br/>
Experimente retirar partes das camadas /definições para ver as mudanças no resultado
```{r, eval = F}
#Seu código aqui


```
<br/>
Experimente mudar o tema. Alguns exemplos*
```{r,eval = F}
theme_minimal()
theme_economist()
```
<br/><br/><br/><br/>_*para ver mais pode escrever_ theme__ e pressionar_ <b>TAB</b>

---
#Outros exemplos genéricos mostrados no arquivo

```{r}
####Exemplo série temporal e autoplot
cageds <- read.csv2("dados/07_caged-2018-2019_sem_dups.csv")

##Formato para ts , série temporal

cageds2 <- cageds %>% pivot_wider(names_from = tipo_de_trabalho,
                                  values_from = saldo_mov)
##A coluna de competencia_declarada é retirada pois será
##especificada manualmente ao criar o objeto de tipo ts
cageds2 <- cageds2[-1]
cageds2 <- ts(cageds2, 
            start=c(2018,1),
              freq = 12)

```
```{r, eval = F}
plot(cageds2)
autoplot(cageds2)
```

---
#Outros exemplos genéricos mostrados no arquivo
```{r}
plot(cageds2)

```
---
#Outros exemplos genéricos mostrados no arquivo
```{r}
autoplot(cageds2)
```