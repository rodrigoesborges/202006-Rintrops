cageds <- read.csv2("dados/07_caged-2018-2019.csv")



 aggregate(saldo_mov ~ substr(competencia_declarada,1,4), cageds, mean )


# colSums(cageds[,-2])/ncol(cageds[,-2])

 #xtabs retorna neste caso soma
xtabs(saldo_mov ~ substr(competencia_declarada,1,4), cageds)

aggregate(. ~ substr(competencia_declarada,1,4), cageds, mean)

library(data.table)
cageds$ano <- as.numeric(substr(cageds$competencia_declarada,1,4))

media_por_ano_cageds <- function(ano) {
  caged_ano <- cageds[cageds$ano == ano,-2]
  colSums(caged_ano/nrow(caged_ano))
  }

lapply(2018:2019,media_por_ano_cageds)

library(tidyverse)
cageds %>% group_by(substr(competencia_declarada,1,4))%>%
  summarize(mean(saldo_mov))


medias <- cbind(2018:2019,c(
  mean(cageds[substr(cageds$competencia_declarada,1,4) == 2018,"saldo_mov"]),
  mean(cageds[substr(cageds$competencia_declarada,1,4) == 2019,"saldo_mov"])
)
)




