---
output: 
  xaringan::moon_reader:
    lib_dir: libs
    nature:
      ratio: 16:9
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
background-image: url(www/slidecaged.jpg)
background-size: cover

<link rel="stylesheet" type="text/css" href="www/formacao.css">


---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE, max.print = 30, digits = 4)
knitr::opts_chunk$set(fig.align = "center", fig.width = 10, fig.height = 6)
library(tidyverse)
library(readxl)
```

# Apresentações

<br>


* Rodrigo Emmanuel Santana Borges

* <a>Dr. em Economia</a> (Universidade Complutense de Madrid, 2015)

* <a>Pós-doutorando</a> do PPGPS/UFES
  
* Teve primeiro contato com <a>R</a> há `r lubridate::year(Sys.Date()) - 2016` anos

---

# Percurso do curso

<br/>
<br/>

1. [Noções](#13) de **R**

2. Como [importar](#30) os dados?

3. Como [transformar/compreender](#35) os dados?

4. Como [visualizar](#75) os dados?

OBS.: as etapas de 2 a 4 serão repetidas ao longo das semanas do curso


---

# Tópicos da semana



1) Como carregar e utilizar:
 - outros scripts
 - pacotes não disponíveis no cran
2) Como adicionar colunas e linhas, calculando indicadores
 - Com base
 - com tidyverse

3) Tabelas - exemplos com data.table e intro a tibbles

4) apresentação mais formal do tidyverse

5) indicador da tabela de população e tabelas *completas* do CAGED


---


# Tópicos de hoje



1) Complementos - calculos agregados por grupo
 
2) Tabelas - exemplos com data.table e intro a tibbles

## === PAUSA PARA EXERCÍCIOS===

4) apresentação mais formal do tidyverse

5) indicadores da tabela de população e tabelas *completas* do CAGED
 - rumo a um indicador de emprego/desemprego municipal _objetivo_



---
POS PAUSA

---
# COM TIDYVERSE - Como <a>transformar</a> os dados

---
background-image: url(www/data-science.png)
background-size: 100%
class: center

# <a>Ciclo</a> de análise de dados

---
background-image: url(www/pipe.jpg)
background-size: 85%
class: center

---

```{r, include = FALSE}
library(tidyverse)
caged2020 <- read_rds("dados/08_dados_caged_2020_05_amostra.rds")
nc_arq_dic <- "dados/Layout Novo Caged Movimentação.xlsx"
regioes <- read_xlsx(nc_arq_dic,sheet = "região")


uefes <- read_xlsx(nc_arq_dic,sheet = "uf")


municipios <- read_xlsx(nc_arq_dic,sheet = "município")



secoes <- read_xlsx(nc_arq_dic,sheet = "seção")


categoria <- read_xlsx(nc_arq_dic,sheet = "categoria")


sexos <- read_xlsx(nc_arq_dic, sheet = "sexo")


etnias <- read_xlsx(nc_arq_dic,sheet = "raçacor")

```


# %>% (pipe)

O <a>pipe</a> está baseado na pintura do artista belga Magritte.

```{r}
valores <- rnorm(10, 10, 2)
mean(abs(diff(valores)))
```

<br>

Vejamos a mesma operação com o <a>pipe</a>

```{r}
# Ceci n'est pas une "operador"

# library(magrittr)
valores %>% 
  diff() %>% 
  abs() %>% 
  mean()
```

---

# <a>dplyr</a> e <a>tidyr</a>

Uma filosofia comum:

1. O primeiro argumento de cada função é um data.frame

2. Os argumentos posteriores descrever o que fazer com o data.frame, usando o nome das variáveis (sem aspas).

3. Toda função retorna um data.frame

.pull-left.img[
  ![](www/dplyr.png)
]

.pull-right.img[
  ![](www/tidyr.png)
]

---
class: center, middle

# Como <a>organizar</a> os dados - tidyr

---
background-image: url(www/data-science.png)
background-size: 100%
class: center

# <a>Ciclo</a> de análise de dados

---

# <a>Princípios</a>

<br><br>

1. Cada <a>variável</a> tem sua própria <a>coluna</a>

2. Cada <a>observação</a> tem sua própria <a>linha</a>

3. Cada <a>unidade observacional</a> tem sua própria <a>tabela</a>

---

# <a>Princípios</a>

<br><br>

> Famílias felizes são todas iguais, cada família infeliz é infeliz 
à sua maneira

.pull.right[Leon Tolstoy]

> Dados arrumados são todos iguais, cada dado bagunçado é bagunçado 
à sua maneira

.pull.right[Hadley Wickham]

---
background-image: url(www/original-dfs-tidy.png)
background-size: 60%
background-position: 50% 90%

# <a>Formas</a> de uma tabela

---
background-image: url(www/tidyr-spread-gather.gif)
background-size: 45%
background-position: 50% 100%

# <a>Transformações</a> de uma tabela

---

# <a>Juntar</a>

```{r, eval=FALSE}
pivot_longer(tabela,c(colunas,da,tabela,a,unir), 
       names_to = como_chamar_o_nome_das_colunas, 
       values_to = como_chamar_a_variavel_com_valores, 
       )
```

---

# <a>Juntar</a>

```{r}
tipo_especifico <- caged2020 %>% 
  mutate(tipo = seq_along(categoria)) %>% 
  select(tipo, categoria, salário:`raçacor`)
  
tipo_especifico
```

---
# <a>Juntar</a>

```{r}
tipo_especifico_tidy <- pivot_longer(caged2020,c(indtrabintermitente,indtrabparcial,indicadoraprendiz), names_to = "tipo_especifico_de_trabalho", values_to = "tipo_especifico_s_n") %>% 
  filter(!is.na(raçacor))
tipo_especifico_tidy
```

---

# <a>Juntar</a>

```{r, eval = FALSE}
tipo_especifico_tidy <- pivot_longer(caged2020, c(indtrabintermitente,indtrabparcial,indicadoraprendiz), names_to = "tipo_especifico_de_trabalho", values_to = "tipo_especifico_s_n") %>% 
  filter(!is.na(`raçacor`))
tipo_especifico_tidy
```

### Como ler

<a>"Quero juntar apenas as colunas _indtrabintermitente_ _indtrabparcial_ e _indicadoraprendiz_ de **caged2020**, em **tipo_especifico_de_trabalho** e **tipo_especifico_s_n**."</a>

```{r, eval = FALSE}
# Não tenha vergonha de pedir ajuda ao R
?pivot_longer
```

---

# <a>Espalhar</a>

```{r, eval=FALSE}
pivot_wider(data.frame, 
       names_from = separe_de_acordo_com_essa_coluna, 
       values_form = separe_os_valores_dessa_coluna)
```

---

# <a>Espalhar</a>

```{r}
pivot_wider(tipo_especifico_tidy, tipo_especifico_de_trabalho, tipo_especifico_s_n)
```

---

# <a>Espalhar</a>

```{r, eval=FALSE}
pivot_wider(tipo_especifico_tidy, tipo_especifico_de_trabalho, tipo_especifico_s_n)
```

### Como ler

<a>"Em **tipo_especifico_tidy**, quero espalhar para cada tipo de **tipo_especifico_de_trabalho** os valores de **tipo_especifico_s_n**."</a>

```{r, eval = FALSE}
# Não tenha vergonha de pedir ajuda ao R
?pivot_wider 
```


